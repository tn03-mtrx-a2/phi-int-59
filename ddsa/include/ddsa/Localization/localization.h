#ifndef _DDSA_INCLUDED_LOCALIZATION_H_
#define _DDSA_INCLUDED_LOCALIZATION_H_
/*
This function initializes the localization,
Inputs:
  Sensor_t EncoderL, Instance of left encoder for the updatePose function to read
  Sensor_t EncoderR, Instance of Right encoder for the updatePose function to read
Returns:
  int, an int based status code
*/
int ddsa_localization_init(Sensor_t EncoderL, Sensor_t EncoderR);
/*
This function shuts down the localization
Returns:
  int, an int based status code
*/
int ddsa_localization_shutdown();
/*
This function will get the Pose of the robot, Pose2D_t is defined in navigator.h
Inputs:
  Pose2D_t* pose, this is where the pose of the robot will be returned
Return:
  int, an int based status code
*/
int ddsa_localization_getPose(Pose2D_t* pose);
/*
This function updates the Pose of the robot, it will be periodically called to update the pose.
Returns:
  int, an int based status code
*/
int ddsa_localization_updatePose();
/*
This function resets the Pose to 0,0,0
Returns:
  int, an int based status code.
*/
int ddsa_localization_resetPose();


#endif