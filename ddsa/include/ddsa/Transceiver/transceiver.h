#ifndef _DDSA_INCLUDED_TRANSCEIVER_H_
#define _DDSA_INCLUDED_TRANSCEIVER_H_
/*
This is the function to initialize transceiver 
Input:
  int Baud_rate, The baud rate at which the comunication should be started
Returns:
 int, The function will return an int status code which will correspond to a particular error
*/
int ddsa_transceiver_init(int Baud_rate);
/*
This is a function for reading the bytes on communication channel
Input:
 char* memory_buffer, this is the pointer to the buffer where the recieved bytes must be stored
 int mem_size, the maximum size of the memory_buffer which will mostly be equal to the size of longest possible response
Returns:
 int, The function will return an int status code which will correspond to a particular error
*/
int ddsa_transceiver_readBytes(unsigned char* memory_buffer, int mem_size); 
/*
This is a function for putting the bytes on communication channel
Input:
 char* bytes, bytes to be put
 int length, length of the packet to be transmitted
Returns:
 int, The function will return an int status code which will correspond to a particular error
*/
int ddsa_transceiver_putBytes( unsigned char* bytes, int length); 
/*
This function is for getting the timeout time for reading bytes
Inputs:
  int* timeInms, this is the pointer where the function will write the timeout time value
Returns:
  int, The function will return an int status code which will correspond to a particular error
*/
int ddsa_transceiver_getTimeout(int* timeInms);
/*
This function is for setting the timeout time for reading bytes
Input:
  int timeInms, the timeout time to be set in milliseconds
Returns:
  int, The function will return an int status code which will correspond to a particular error
*/
int ddsa_transceiver_setTimeout(int timeInms);
/*
This function deletes the transceiver object, as there is only one instance we do not need any inputs
Returns:
  int, The function will return an int status code which will correspond to a particular error
*/
int ddsa_transceiver_shutdown();
#endif