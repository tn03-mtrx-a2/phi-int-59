#ifndef _DDSA_INCLUDED_SENSOR_MANAGER_H_
#define _DDSA_INCLUDED_SENSOR_MANAGER_H_

typedef struct _ddsa_sensormanager
{
    Sensor_t* (*getSensor)(uint32_t SensorId); // Checks if the required sensor is registered with sensorManager, Returns the Instance of sensor or NULL
    int (*registerSensor)(Sensor_t Sensor); // Registers the sensor
}SensorManager_t;

SensorManager_t* ddsa_getSensorManager();

/*
This function initiates the sensor manager, the array to store sensors
Returns:
  int, an int based status code
*/
int ddsa_sensorManager_init();
/*
This function shuts down the sensor manager and also destroys the array 
Returns:
  int, an int based status code
*/
int ddsa_sensorManager_shutdown();
/*
This function registers the sensor with sensor manager
Inputs:
  Sensor_t Sensor, the instance of the sensor to be registered
Returns:
  int, an int based status code
*/
int ddsa_sensorManager_registerSensor(Sensor_t Sensor);
/*
This function returns if the required sensor is registered with sensor manager
Input:
  uint32_t SensorId, the Id of the sensor to be checked
Returns:
  int, an int based status code
*/
int ddsa_sensorManager_hasSensor(uint32_t SensorId);
/*
This function gets the required sensor from the array 
Input:
  Sensor_t* Sensor, This is where sensor will be returned
  uint32_t SensorId, This is the ID of the sensor required
Returns:
  int, an int based status code
*/
int ddsa_sensorManager_getSensor(Sensor_t* Sensor,uint32_t SensorId);
/*
This function gets the size of the array that sensors manager manages
Input:
  int* Size, This is where the size will be returned
Returns:
  int, an int based status code
*/
int ddsa_sensorManager_getManagerSize(int* Size);
#endif