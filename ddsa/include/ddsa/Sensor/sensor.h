#ifndef _DDSA_INCLUDED_SENSOR_H_
#define _DDSA_INCLUDED_SENSOR_H_

typdef union
{
    uint32_t number;
    char chars[4];
}SensorId_t;

typdef struct _ddsa_sensor  
{
    SensorId_t Id;
    int  type;
    int (*readSensor)(void* reading);
    int (*writeParam)(uint32_t type, void* data);
}Sensor_t;

#endif