#ifndef _DDSA_INCLUDED_MOTOR_CONTROLLER_H_
#define _DDSA_INCLUDED_MOTOR_CONTROLLER_H_

#define GEAR_REDUCTION_RATIO 3

typedef struct _ddsa_motor_cntroller
{
   int id; //ID of the motor controller
   /*
   This function sets the speed of the motor, taking into consideration the reduction ratio of the pulleys. It will have PID controller to ensure error minimization
   and will also require access to the encoder ticks.
   Inputs:
   float speed, the speed to be set
   Returns:
   int, an int based status code
   */
   int (*set_speed)(float speed); 
}Motor_controller_t;

/*
This function creates the required object of the class and returns it
Inputs:
  Motor_controller_t* Motor_controller, this is where the motor controller will be written
  int id, This is the id of the motor controller
Returns:
  int, an int based status code.
*/
int ddsa_getMotor_controller(Motor_controller_t* Motor_controller,int id);

#endif