#ifndef _DDSA_INCLUDED_NAVIGATION_H_
#define _DDSA_INCLUDED_NAVIGATION_H_

typedef struct _ddsa_pose
{
 float x;
 float y;
 float theta;
}Pose2D_t;

/*
This function initiates navigator
Returns:
  int, An int based status code.
*/
int ddsa_mavigator_init();
/*
This function shuts down navigator
Returns:
  int, An int based status code.
*/
int ddsa_navigator_shutdown();
/*
This function makes the robot move to its goal position
Input:
  Pose2D_t end_pose, the end goal co-ordinates.
Returns:
  int, An int based status code.
*/
int ddsa_navigator_move(Pose2D_t* result,Pose2D_t end_pose);

#endif