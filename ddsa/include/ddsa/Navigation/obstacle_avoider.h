#ifndef _DDSA_INCLUDED_OBSTACLE_AVOIDER_H_
#define _DDSA_INCLUDED_OBSTACLE_AVOIDER_H_

/*
This function initiates Obstacle avoider, should be initialized after navigator
Returns:
  int, an int based status code.
*/
int ddsa_avoider_init();
/*
Thsi function shuts down obstacle avoider,
Returns:
  int, an int based status code.
*/
int ddsa_avoider_shutdown();
/*
This function calculates an obstacle free trajectory by reading the sensor values. 
The curve will be modified by changing control points to make the curve obstacle free
Inputs:
  Point_t* controlpt1, The first control point of the trajectory, if the trajectory is already obtsacle free or there are no sensors, no changes will be done
  Point_t* controlpt2, The second control point of the trajectory, if the trajectory is already obtsacle free or there are no sensors, no changes will be done
  Sensor_t* sensor, An array of sensors required by avoider
  int sensor_count, Number of sensors in array
Returns:
  int, an int based status code, which will tell if the trajectory is modified or is the same. 
*/
int ddsa_avoider_avoid(Point_t* controlpt1,Point_t* controlpt2,Sensor_t* sensor,int sensor_count);

#endif