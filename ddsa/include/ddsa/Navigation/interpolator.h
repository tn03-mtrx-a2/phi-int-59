#ifndef _DDSA_INCLUDED_INTERPOLATOR_H_
#define _DDSA_INCLUDED_INTERPOLATOR_H_

typedef struct _ddsa_point // Struct for a 2D point
{
  float x;
  float y;
}Point_t;

typedef struct _ddsa_velocity // Struct for 2D Velocity Vector
{
  float i;
  float j;
}VelVector_t;

/*
This function initiates interpolator, Interpolator should be initialized after navigator
Returns:
  int, an int based status code.
*/
int ddsa_interpolator_init();
/*
This function shuts down the interpolator,
Returns:
  int, an int based status code.
*/
int ddsa_interpolator_shutdown();
/*
This function calculates the control points when start and end points and Initial and final velocities are given
Inputs:
  Point_t* controlPt1, This is where first control point (i.e. second point of curve) will be written
  Point_t* controlPt2, This is where second control point (i.e. third point of curve) will be written
  Point_t start_pt, This is the start point of the trajectory (current position of robot)
  Point_t end_pt, This is the end point of the trajectory (goal position)
  VelVector_t initial, This is the initial velocity vector of the robot
  VelVector_t final, This is the final velocity vector of the robot
Returns:
  int, an int based status code.
*/
int ddsa_interpolator_calculateControlPts(Point_t* controlPt1,Point_t* controlPt2,Point_t start_pt, Point_t end_pt, VelVector_t initial, VelVector_t final);
/*
This function calculates the cubic bezeir curve given all 4 points.
Inputs:
  Point_t* curve, The array of curve points will be written here 
  Point_t start_pt, This is the start point of the trajectory (current position of robot)
  Point_t control_pt1, This is the first control point
  Point_t control_pt2, This is the second control point
  Point_t end_pt, This is the end point of the trajectory (goal position)
  int no_of_pts, The resolution of the curve, That is how many points on the curve are required
Returns:
  int, an int based status code.
*/
int ddsa_interpolator_calculateCurve(Point_t* curve,Point_t start_pt, Point_t control_pt1, Point_t control_pt2, Point_t end_pt, int no_of_pts)
/*
This function interpolates over the curve, calculates the Radius of curvature of 3 consecutive points and stores it in array
Inputs:
  float* R_list, This is the list of Radii of curvature calculated by function.
  Point_t* curve, This is the array of curve points
  int no_of_pts, This is how many points on the curve are calculated.
*/
int ddsa_interpolator_interpolate(float* R_list,Point_t* curve, int no_of_pts);

#endif 