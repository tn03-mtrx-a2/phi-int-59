#ifnedf _DDSA_INCLUDED_COMMAND_MANAGER_H_
#define _DDSA_INCLUDED_COMMAND_MANAGER_H_

//Command Manager should be initialized After Communicator
/*
As there will be only one instance of Command Manager, We'll always know which one to work with
This is the initialize function, it also creates the register to store the commands.
RETURNS:
  int, This is the int status about the execution of this function
*/
int ddsa_command_manager_init();

/*
This function appends the command in the register
INPUTS:
   Command_t* command, the command to be entered
RETURNS:
   int, status code, this may return the successful operation code or any error occured.(Like Register full etc.)
*/
int ddsa_command_manager_registerCommand(Command_t* command);

/*
This function searches if a command is present in the register
INPUTS:
   Command_t* command, the command to be searched
   uint8_t length_of_manager, the length of the manager
RETURNS:
   int, status code, this may return the successful operation code or any error occured.(Like Not found etc.)
*/
int ddsa_command_manager_hasCommand(Command_t* command, uint8_t length_of_manager);

/*
This function gets the size of the manager
INPUTS:
   uint8_t* lenth_of_manager, the pointer to uint8_t where the length will be written
RETURNS:
   int, status code, this may return the successful operation code or any error occured.(Like Manager not initialized etc.)
*/
int ddsa_command_manager_getManagerSize(uint8_t* length_of_manager);

/*
This function shuts down the command manager, destroys the register etc.
RETURNS:
  int, This is the int status about the execution of this function
*/
int ddsa_command_manager_Shutdown();
#endif