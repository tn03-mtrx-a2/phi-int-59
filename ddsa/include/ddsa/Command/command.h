#ifndef _DDSA_INCLUDE_COMMAND_H_
#define _DDSA_INCLUDE_COMMAND_H_

typedef uint32_t CommandId_t;
typedef int32_t (*CommandExec_fp)(void* data, int32_t dataId);

typedef struct _ddsa_command
{
    CommandId_t id;
    CommandExec_fp exec;
}Command_t;

#endif