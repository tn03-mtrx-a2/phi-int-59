#ifndef _DDSA_INCLUDED_V_AND_W_CONTROLLER_H_
#define _DDSA_INCLUDED_V_AND_W_CONTROLLER_H_

struct ddsa_VWControllerConfig
{
  float wheel_radius; 
  float wheel_separation;
  void* extensible_data; // This is so that the application can be extended to different drive types and not just for DDMR
};

/*
This function initiates V and W controller
Inputs:
  struct VWControllerConfig, This struct has all the required geometry information
Returns:
  int, an int based status code
*/
int ddsa_VandWcntrller_init(struct ddsa_VWControllerConfig VWControllerConfig);
/*
This function shuts down V and W controller
Returns:
  int, an int based status code
*/
int ddsa_VandWcntrller_shutdown();
/*
This function returns the value of linear and angular velocity that the bot is currently following
Inputs:
  float* V, a pointer to the place where the linear velocity will be written
  float* W, a pointer to the place where the angular velocity will be written
Returns:
  int, an int based status code
*/
int ddsa_VandWcntrller_getVandW(float* V, float* W);
/*
Thsi function maps the linear and angular velocity of robot to angular velocity of each wheels
Inupts:
  float* Wl, This is the location where the angular velocity for the left wheel will be stored
  float* Wr, This is the location where the angular velocity for the right wheel will be stored
  float V, the linear velocity of the robot
  float W, the angular velocity of the robot
Returns:
  int,an int based status code
*/
int ddsa_VandWcntrller_calculateWheelSpeed(float* Wl, float* Wr, float V, float W);

#endif