#ifndef _DDSA_RESPONSE_H_
#define _DDSA_RESPONSE_H_

typedef uint32_t ResponseId_t; // Mostly will be same as the received RequestID to which this response is presented
typedef char ResponseType_t; // Type of Response, maybe sensor data, or progress of navigation
struct _ddsa_data (uint8_t* data, unsigned int length); // The data to be sent

typedef struct _ddsa_response
{
    ResponseId_t Id;
    ResponseType_t response_type;
    struct _ddsa_data response_data;
}Response_t;

#endif