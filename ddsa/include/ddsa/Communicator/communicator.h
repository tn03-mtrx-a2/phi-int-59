#ifndef _DDSA_COMMUNICATOR_H_
#define _DDSA_COMMUNICATOR_H_

/*
This is function to initialize communicator
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_init();
/*
This is function to delete communicator
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_shutdown();
/*
This is a function which tells if it has received a packet or not
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_hasPkt();
/*
This is a function which begins process to write the packet
Inputs:
 uint8_t* pkt_buffer, a buffer where the packet will be written
 int length, the length of the defined buffer
 char packet_type, this is a packet type, it may be request or response
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_beginPktWrite(uint8_t* pkt_buffer, int length, char packet_type);
/*
This function will end the packate write process
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_endPktWrite();
/*
This function will write the given data in UINT8 as payload
Inputs:
  u8int_t* data, this is the pointer to data which is supposed to be written
  int length, the length of the data 
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_writeU8(uint8_t* data, int length)
/*
This function will write the given data in UINT16 as payload
Inputs:
  uint16_t* data, this is the pointer to data which is supposed to be written
  int length, the length of the data 
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_writeU16(uint16_t* data, int length)
/*
This function will write the given data in FLOAT as payload
Inputs:
  float* data, this is the pointer to data which is supposed to be written
  int length, the length of the data 
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_writeF(float* data, int length)
/*
This function will start the packet reading process
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_beginPktRead();
/*
This function will end the packet reading process
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_endPktRead();
/*
This function will read the payload if it is UINT8 datatype
Input:
  uint8_t* buffer, this is where the read data will be stored
  int length, length of the defined buffer
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_readU8(uint8_t* buffer, int length)
/*
This function will read the payload if it is UINT16 datatype
Input:
  uint16_t* buffer, this is where the read data will be stored
  int length, length of the defined buffer
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_readU16(uint16_t* buffer, int length)
/*
This function will read the payload if it is FLOAT datatype
Input:
  float* buffer, this is where the read data will be stored
  int length, length of the defined buffer
Returns:
 int, int status code corresponding to error or success
*/
int ddsa_communicator_readF(float* buffer, int length)


#endif
