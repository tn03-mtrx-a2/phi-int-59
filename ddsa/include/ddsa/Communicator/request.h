#ifndef _DDSA_REQUEST_H_
#define _DDSA_REQUEST_H_

typedef uint32_t RequestId_t;
typedef char RequestType_t;
struct _ddsa_buffer {uint8_t* data, unsigned int size};
 
typedef struct _ddsa_request
{
    RequestId_t Id;
    RequestType_t type;
    struct _ddsa_buffer buffer;
}Request_t;

#endif